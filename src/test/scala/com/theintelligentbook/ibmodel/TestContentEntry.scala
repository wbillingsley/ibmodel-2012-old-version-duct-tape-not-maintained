package com.theintelligentbook.ibmodel

import org.junit.{Test, Before, AfterClass, BeforeClass}
import com.wbillingsley.handy._
import Ref._
import org.junit.Assert._
import com.theintelligentbook.ibmodel.mongo._


object TestContentEntry { 
  
  var fred:Ref[Reader] = RefNone  
  var book:Ref[Book] = RefNone   
  var ce1:Ref[ContentEntry] = RefNone  
  var ce2:Ref[ContentEntry] = RefNone
  var pres1:Ref[Presentation] = RefNone
  
  @BeforeClass
  def init () = {
    com.theintelligentbook.ibmodel.Settings.init()
    DAO.getCollection(ContentEntryDAO.collName).drop()
    
    fred = ReaderModel.findOrCreateByIdent("test", "test", "makePresOfTwo")
    for (reader <- fred) { 
      reader.nickname = Some("TestPresReader")
      reader.siteRoles += SiteRole.Author
      ReaderDAO.save(reader)
    }
        
    val tok = Approval(fred)
    book = BookModel.createBook(tok, "TestContentEntry", "TestContentEntry", "TestContentEntry book")    
    ce1 = ExternalsModel.newWebsiteEntry(book, tok, "http://www.bing.com")
    ce2 = ExternalsModel.newWebsiteEntry(book, tok, "http://www.google.com")
    pres1 = PresentationModel.makePresentation(book, tok, ce1)
    
    
  }

  @AfterClass
  def close () = {
  }


}

class TestContentEntry {
  
  /**
   * Tests resolving a presentation (sequence) and content entry to show, given a 
   * presentation and content entry as context. 
   */
  @Test def presAndCe = {
    
    import TestContentEntry._

    val tok = Approval(fred)      

    val rpPres = Ref.fromOptionId(classOf[Presentation], pres1.getId)
    
    val rpNothing = RefNone
    val rpMissed = new RefById(classOf[Presentation], "")
    
    val rcFirstInPres = Ref.fromOptionId(classOf[ContentEntry], ce1.getId)
    val rcNothing = RefNone
    val rcPresItself = pres1.flatMap(_.ce)
    val rcMissed = new RefById(classOf[ContentEntry], "")
    
    /*
     * Given presentation and failed reference, get presentation and first slide
     */
    val(rc1, rp1) = ContentModel.presAndCE(rcMissed, rpPres)    
    assertEquals("First presAndCe test returned wrong presentation",
      rpPres.getId,
      rp1.getId
    )
    assertEquals("First presAndCe test returned wrong ce",
      rpPres.flatMap(_.entries.first).getId,
      rc1.getId
    )
    
    /*
     * Given presentation and its own CE, get presentation and first slide
     */
    val(rc2, rp2) = ContentModel.presAndCE(rcPresItself, rpPres)    
    assertEquals("Second presAndCe test returned wrong presentation",
      rpPres.getId,
      rp2.getId
    )
    assertEquals("Second presAndCe test returned wrong ce",
      rpPres.flatMap(_.entries.first).getId,
      rc2.getId
    )
    
    /*
     * Given first slide and no presentation, get presentation and first slide
     */
    val(rc3, rp3) = ContentModel.presAndCE(rcFirstInPres, rpNothing)    
    assertEquals("Third presAndCe test returned wrong presentation",
      rpPres.getId,
      rp3.getId
    )
    assertEquals("Third presAndCe test returned wrong ce",
      rpPres.flatMap(_.entries.first).getId,
      rc3.getId
    )    
    
    
  }
 
  
}