package com.theintelligentbook.ibmodel

import com.wbillingsley.handy._
import com.theintelligentbook.ibmodel.mongo.DAO

/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 9/06/12
 * Time: 10:54 PM
 * To change this template use File | Settings | File Templates.
 */

object Settings {

  val testDBname = "ib_mongo_test"

  def init () = {
    
    RefById.lookUpMethod = new RefById.LookUp {
      def lookup[T](r:RefById[T, _]) = DAO.resolve(r)
    }
    RefManyById.lookUpMethod = new RefManyById.LookUp {
      def lookup[T](r:RefManyById[T, _]) = DAO.resolveMany(r)
    }
    
    DAO.database = Settings.testDBname
    DAO.initConn()
  }

}
