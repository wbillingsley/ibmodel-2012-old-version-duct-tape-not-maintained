package com.theintelligentbook.ibmodel

import org.junit.{Test, Before, AfterClass, BeforeClass}
import com.wbillingsley.handy._
import Ref._
import org.junit.Assert._
import com.theintelligentbook.ibmodel.mongo._


object TestPresentation { 
  
  var fred:Ref[Reader] = RefNone
  var book:Ref[Book] = RefNone  
  
  @BeforeClass
  def init () = {
    com.theintelligentbook.ibmodel.Settings.init()
    DAO.getCollection(PresentationDAO.collName).drop()
    
    fred = ReaderModel.findOrCreateByIdent("test", "test", "makePresOfTwo")
    for (reader <- fred) { 
      reader.nickname = Some("TestPresReader")
      reader.siteRoles += SiteRole.Author
      ReaderDAO.save(reader)
    }
        
    book = BookModel.createBook(Approval(fred), "TestPresentation", "TestPresentation", "TestPresentation book")
    
  }

  @AfterClass
  def close () = {
  }


}

class TestPresentation {
  
  /**
   * Tests making a sequence of two content entries
   */
  @Test def makePresOfOne = {
    
    import TestPresentation._

    val tok = Approval(fred)      
    val ce1 = ExternalsModel.newWebsiteEntry(book, tok, "http://www.bing.com")    
    val p = PresentationModel.makePresentation(book, tok, ce1)
    
    val entries = p.flatMap(_.entries.getIds.toRefMany)

    // Test that the presentation has our single item in the list
    assertEquals(
      "Failed creating a presentation of a single entry",
      List(ce1.toOption.get.id),
      entries.fetch.toList
    )
    
    
  }
 
  
}