package com.theintelligentbook.ibmodel

import com.wbillingsley.handy._
import mongo.{ContentEntryDAO, ContentEntry, Reader, Book}
import org.slf4j.LoggerFactory


/**
 * Single import to define various types.
 * Inspired by com.mongodb.casbah.Imports
 */
object Imports {

  type BookRef = Ref[Book]
  type ReaderRef = Ref[Reader]
  type EntryRef = Ref[ContentEntry]
  type PermTok = Approval[Reader]



  def tryo[T](f: => T) = {
    try {
      RefItself(f)
    } catch {
      case ex:Exception => RefFailed(ex)
    }
  }

}

trait Loggable {
  val logger = LoggerFactory.getLogger(this.getClass)

}
