/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.theintelligentbook.ibmodel

import mongo._
import Imports._
import com.wbillingsley.handy._
import SecurityModel._

import _root_.scala.collection.mutable.Buffer

import _root_.org.joda.time.{DateTime, Interval}

object ActionLogModel extends Loggable {

  val MAX_DATE = new java.util.Date(java.lang.Long.MAX_VALUE)
  val MIN_DATE = new java.util.Date(0L)

  /**
   * Logs an action.
   */
  def logAction(
    ce:Ref[ContentEntry] = RefNone,
    book:Ref[Book] = RefNone,
    reader:Ref[Reader] = RefNone,
    session:Option[String] = None,
    action:Option[String] = None,
    kind:Option[String] = None,
    site:Option[String] = None,
    adjs:Option[Set[String]] = None,
    nouns:Option[Set[String]] = None,
    topics:Option[Set[String]] = None,
    extra:Option[String] = None
  ) {
    val ceOpt = ce.toOption
    val logE = ALogEDAO.newALogE(ce=ce, book=book, reader=reader, session=session, action=action,
      kind=kind.orElse(ceOpt.map(_.kind)),
      site=site.orElse(ceOpt.map(_.site)),
      adjs=adjs.getOrElse(ceOpt.map(_.adjectives).getOrElse(Set.empty[String])),
      nouns=nouns.getOrElse(ceOpt.map(_.nouns).getOrElse(Set.empty[String])),
      topics=topics.getOrElse(ceOpt.map(_.topics).getOrElse(Set.empty[String])),
      extra=extra
    )
    ALogEDAO.save(logE)
  }

  def updateAggregatedEvents(b:Ref[Book], approval:Approval[Reader]) {
    for (a <- approval ask ViewStats(b)) {
      ALogEDAO.updateStatsByDay(b)
    }
  }
  
  def aggregatedEvents(b:Ref[Book], approval:Approval[Reader]) = {
    (approval ask ViewStats(b)) flatMap { a => 
      RefTraversableOnce(AggEventsDAO.aggEventsJson(b))
    }
  }
  
}
