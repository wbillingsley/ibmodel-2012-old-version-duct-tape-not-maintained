package com.theintelligentbook.ibmodel

import mongo._
import Imports._
import com.wbillingsley.handy._
import Ref._

import _root_.java.util.Date

import _root_.com.theintelligentbook.ibmodel.RefConversions._


object SecurityModel {

  /* Our permissions */
  
  import com.mongodb.casbah.Imports._
  import HasObjectId.GetsObjectId

  /**
   * Create a book
   */
  case object CreateBook extends Perm[Reader] {    
    def resolve(prior:Approval[Reader]) = {
      (for (r <- prior.who if r.siteRoles contains SiteRole.Author) yield {
        Approved("You are an Author on this site")
      }) orIfNone Refused("You must be an Author on this site to create books.")
    }
  }

  case class EditBook(book:Ref[Book]) extends PermOnIdRef[Reader, Book](book) {
    def resolve(prior:Approval[Reader]) = hasRole(book, prior.who, BookRole.Administrator)
  }

  /**
   * Create invitations to a book
   * @param bookRef
   */
  case class Invite(book:Ref[Book]) extends PermOnIdRef[Reader, Book](book)  {
    def resolve(prior:Approval[Reader]) = hasRole(book, prior.who, BookRole.Administrator)
  }
  
  /**
   * Read a book or an entry in a book
   * @param bookRef
   */
  case class Read(book:Ref[Book]) extends PermOnIdRef[Reader, Book](book) {
    def resolve(prior:Approval[Reader]) = {
      book flatMap { b =>
        b.signupPolicy match {
	        case BookSignupPolicy.open => Approved("Anyone may read this book").itself
	        case BookSignupPolicy.loggedIn => {
	          (prior.who map {
	            w => Approved("Logged in readers may read this book")
	          }) orIfNone Refused("You must log in to read this book")
	        }
	        case _ => hasRole(book, prior.who, BookRole.Reader)          
        }
      } orIfNone Refused("Book not found")
    }
  }
  
  case class ReadEntry(entry:Ref[ContentEntry]) extends PermOnIdRef[Reader, ContentEntry](entry) {
    def resolve(prior:Approval[Reader]) = prior ask Read(entry flatMap(_.book))  
  }

  /**
   * Read a book or an entry in a book
   * @param bookRef
   */
  case class Chat(book:Ref[Book]) extends PermOnIdRef[Reader, Book](book) {
    def resolve(prior:Approval[Reader]) = {
      book flatMap { b =>
	    b.chatPolicy match {
	      case BookChatPolicy.allReaders => prior ask Read(book)
	      case _ => hasRole(book, prior.who, BookRole.Chatter)
        }
      } orIfNone Refused("Book not found")
    }
  }

  /**
   * Upvote or downvote an item in a book
   */
  case class Vote(book:Ref[Book]) extends PermOnIdRef[Reader, Book](book) {
    def resolve(prior:Approval[Reader]) = {
      prior.who flatMap (r => prior ask Read(book))
    } orIfNone Refused("Only logged in readers can vote")    
  }
  
  /**
   * Whether a user can like/dislike/comment on a content entry
   */
  case class Comment(entry:Ref[ContentEntry]) extends PermOnIdRef[Reader, ContentEntry](entry) {
    def resolve(prior:Approval[Reader]) = {
    	prior ask Chat(entry flatMap(_.book))
    }
  }
  
  /**
   * Upvote or downvote a content entry
   */
  case class VoteOnEntry(entry:Ref[ContentEntry]) extends PermOnIdRef[Reader, ContentEntry](entry) {
    def resolve(prior:Approval[Reader]) = {
      prior.who.flatMap { r => 
        entry.flatMap { e => 
          if (e.votes.exists(v => v.addedBy.getId == r.itself.getId)) {
            RefFailed(Refused("You have already voted"))
          } else {
            prior ask Read(e.book)
          }
        } 
      } orIfNone Refused("Only logged in readers can vote")    
    }
  }
  
  case class RegisterUsingInvite(book:Ref[Book]) extends PermOnIdRef[Reader, Book](book) {
    def resolve(prior:Approval[Reader]) = {
      Approved("Anyone may register by invitation").itself
    }
  }

  /**
   * Add content to a book
   * @param bookRef
   */
  case class AddContent(book:Ref[Book]) extends PermOnIdRef[Reader, Book](book) {
    def resolve(prior:Approval[Reader]) = hasRole(book, prior.who, BookRole.Author)
  }

  /**
   * Upload media to a book
   */
  case class UploadMedia(book:Ref[Book]) extends PermOnIdRef[Reader, Book](book) {
    def resolve(prior:Approval[Reader]) = prior ask AddContent(book)
  }

  
  /**
   * Protect content or edit protected content in a book
   * @param bookRef
   */
  case class ProtectContent(book:Ref[Book]) extends PermOnIdRef[Reader, Book](book) {
    def resolve(prior:Approval[Reader]) = hasRole(book, prior.who, BookRole.Moderator)
  }

  case class ProtectEntry(entry:Ref[ContentEntry]) extends PermOnIdRef[Reader, ContentEntry](entry) {
    def resolve(prior:Approval[Reader]) = prior ask ProtectContent(entry flatMap(_.book))
  }
  
  
  /**
   * View collected statistics
   * @param bookRef
   */
  case class ViewStats(book:Ref[Book]) extends PermOnIdRef[Reader, Book](book) {
    def resolve(prior:Approval[Reader]) = hasRole(book, prior.who, BookRole.Moderator)
  }

  /**
   * Edit a content entry
   */
  case class EditContent(entry:Ref[ContentEntry]) extends PermOnIdRef[Reader, ContentEntry](entry) {
    def resolve(prior:Approval[Reader]) = {
      entry flatMap { e =>
        if (e.protect) {
          hasRole(e.book, prior.who, BookRole.Moderator)
        } else {
          hasRole(e.book, prior.who, BookRole.Author)
        }
      } orIfNone Refused("Entry not found")
    }
  }

  case class EditPresentation(pres:Ref[Presentation]) extends PermOnIdRef[Reader, Presentation](pres) {
    def resolve(prior:Approval[Reader]) = {
      pres flatMap { p => 
        if (p.protect) {
          hasRole(p.book, prior.who, BookRole.Moderator)
        } else {
          hasRole(p.book, prior.who, BookRole.Author)
        }        
      } orIfNone Refused("Presentation not found")
    }
  }
  
  case class EditWikiPage(wpRef:Ref[WikiPage]) extends PermOnIdRef[Reader, WikiPage](wpRef) {
    def resolve(tok:PermTok) = {
      wpRef flatMap { wp =>
        if (wp.protect) {
          hasRole(wp.book, tok.who, BookRole.Moderator)
        } else {
          hasRole(wp.book, tok.who, BookRole.Author)
        }
      } orIfNone Refused("Wiki page not found") 
    }
  }


  case class EditPoll(pollRef:Ref[Poll]) extends PermOnIdRef[Reader, Poll](pollRef) {
    def resolve(tok:PermTok) = {
      pollRef flatMap { poll =>
        if (poll.protect) {
          hasRole(poll.book, tok.who, BookRole.Moderator)
        } else {
          hasRole(poll.book, tok.who, BookRole.Author)
        }
      } orIfNone Refused("Poll not found")
    }
  }
  
  case class RespondToPoll(poll:Ref[Poll]) extends PermOnIdRef[Reader, Poll](poll) {
	def resolve(prior:PermTok) = prior ask Chat(poll flatMap(_.book))
  }

  case class RespondToTextPoll(poll:Ref[TextPoll]) extends PermOnIdRef[Reader, TextPoll](poll) {
	def resolve(prior:PermTok) = prior ask Chat(poll flatMap(_.book))
  }
  
  
  case class ViewPollResults(poll:Ref[Poll]) extends PermOnIdRef[Reader, Poll](poll) {
    def resolve(prior:PermTok) = {
      val a = for (p <- poll) yield {
        p.resultsVisibility match {
          case PollResultsVisibility.secret => prior ask EditPoll(p.itself)
          case PollResultsVisibility.afterVote => {
            if (PollResponseDAO.findVote(p.itself, prior.who, None).isEmpty) {
              RefFailed(Refused("You can only view the results after voting"))
            } else {
              Approved("You have voted and may view the results").itself
            }
          }
          case _ => prior ask Read(p.book)
        }       
      }
      a.flatten
    }
  }
  
  case class ViewTextPollResults(poll:Ref[TextPoll]) extends PermOnIdRef[Reader, TextPoll](poll) {
    def resolve(prior:PermTok) = {
      prior ask Read(poll.flatMap(_.book))	
    }
  }
  

  /**
   * View media file
   */
  case class ViewMediaFile(mf:Ref[MediaFile]) extends PermOnIdRef[Reader, MediaFile](mf) {
    def resolve(prior:Approval[Reader]) = {
      mf flatMap { m =>
        if (m.public) {
          Approved("Media file is public").itself
        } else {
          prior ask Read(m.book)
        }
      } orIfNone Refused("Media file not found")
    }
  }


  /**
   * Collects information on permissions to send back to the client
   */
  def permsOnEntry(entryRef:EntryRef, readerRef:ReaderRef) = {
    val roles = getRoles(entryRef.book, readerRef)
    Seq(
      // TODO "chat" -> check the book details for whether you need Reader to be able to chat
      "add" -> roles.contains(BookRole.Author),
      "edit" -> roles.contains(BookRole.Author),  // TODO: Check whether entry is protected, in which case requires Moderator
      "protect" -> roles.contains(BookRole.Moderator)
    )
  }


  /*-------------------------
  * Invitation and registration methods
  */

  def useInviteByBookAndCode(readerRef:ReaderRef, bookRef:Ref[Book], code:String) = {
    useInvite(readerRef, BookInvitationDAO.inviteByBookAndCode(bookRef, code))
  }
    
  def useInvite(reader:Ref[Reader], invite:Ref[BookInvitation]) = {
    reader flatMap { r => invite flatMap { i =>
      BookInvitationDAO.useInvite(r, i)
    }}
  }

  def getRoles(book: BookRef, reader: ReaderRef) = {
    RegistrationDAO.findByReaderAndBook(reader, book).flatMap(_.roles).toSet
  }
  
  def hasRole(book:Ref[Book], reader:Ref[Reader], role:BookRole):Ref[Approved] = {
    if (RegistrationDAO.findByReaderAndBook(reader, book).isEmpty) {
      Refused("You do not have the role " + role.toString + " for this book")
    } else Approved("You have the role " + role.toString + " on this book")
  }

}
