package com.theintelligentbook.ibmodel;

/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 11/06/12
 * Time: 7:47 PM
 * To change this template use File | Settings | File Templates.
 */
public enum CEKinds {
    YouTube,
    GoogleSlides,
    Poll,
    TextPoll,
    URL, // To avoid overloading the 'URL' class this corresponds to 'Website'
    MediaFile,
    MarkupWikiPage,
    WikiPage,
    Presentation
}
