package com.theintelligentbook.ibmodel

import com.wbillingsley.handy._
import mongo._
import RefConversions._
import Imports._
import SecurityModel._

object BookModel {

  def myBooks(reader:Ref[Reader]):Seq[Ref[Book]] = {
    reader.fetch match {
      case RefItself(r) => {
        RegistrationDAO.findByReader(r.id).map(_.book)
      }
      case _ => Seq.empty[Ref[Book]]
    }
  }

  def allBooks():Seq[Ref[Book]] = {
    BookDAO.allBooks.map(RefItself(_))
  }
  
  def listedBooks:Seq[Ref[Book]] = {
    BookDAO.listedBooks.map(RefItself(_))
  }

  def createBook(approval:Approval[Reader], title:String, shortDesc:String, pageOneContent:String):Ref[Book] = {
    for (a <- approval ask CreateBook) yield {
      val b = BookDAO.newBook
      b.title = Some(title)
      b.shortDescription = Some(shortDesc)
      BookDAO.save(b)
      
      val reader = approval.who

      val reg = RegistrationDAO.newRegistration(RefItself(b), reader, Set.empty[BookRole] ++ BookRole.values)
      RegistrationDAO.save(reg)

      val ce = ContentEntryDAO.newContentEntry(RefItself(b), reader, CEKinds.WikiPage.toString)
      val wp = WikiPageDAO.newWikiPage(RefItself(b), RefItself(ce), reader, RefNone)
      wp.content = pageOneContent
      WikiPageDAO.save(wp)

      ce.nouns = ce.nouns + "Contents"
      ce.topics = ce.topics + "Page One"
      ce.refVal = Some(wp.id.toString)
      ContentEntryDAO.save(ce)

      b
    }
  }

  def saveBook(book:Ref[Book], approval:Approval[Reader]) = {
    for (b <- book; a <- approval ask EditBook(book)) yield {
      BookDAO.save(b)
      b
    }
  }

  def createInvite(b:BookRef, approval:PermTok, code:Option[String], roles:Set[BookRole], limitedNumber:Boolean, howMany:Int) = {
    for (a <- approval ask Invite(b)) yield {
      val i = BookInvitationDAO.newBookInvitation(b)
      for (c <- code) {
        i.code = Some(c)
      }
      i.roles = roles
      i.limitedNumber = limitedNumber
      i.number = howMany
      i.remaining = howMany
      BookInvitationDAO.save(i)
      i
    }
  }

}
