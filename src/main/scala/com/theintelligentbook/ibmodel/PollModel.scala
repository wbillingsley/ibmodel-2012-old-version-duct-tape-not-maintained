/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.theintelligentbook.ibmodel

import mongo._
import Imports._
import com.wbillingsley.handy._
import SecurityModel._
import Ref._


object PollModel {


  /**
   * Adds a new ContentEntry for a wiki page
   * @param bookRef
   * @param tok
   * @param content
   * @param name
   * @param shortDescription
   * @param adjectives
   * @param nouns
   * @param topics
   * @param showFirst
   * @param protect
   * @return
   */
  def newPollEntry(book:Ref[Book], approval:Approval[Reader],
    name:Option[String], shortDescription:Option[String],
    adjectives:Set[String], nouns:Set[String], topics:Set[String], highlightTopic:Option[String],
    showFirst:Boolean, protect:Boolean
  ):Ref[ContentEntry] = {
    for (a <- approval ask AddContent(book)) yield {
      val ce = ContentEntryDAO.newContentEntry(book, approval.who, CEKinds.Poll.toString)
      val p = PollDAO.newPoll(book, RefItself(ce), approval.who)

      ce.refVal = Some(p.id.toString)
      ce.name = name
      ce.shortDescription = shortDescription
      ce.adjectives = adjectives
      ce.nouns = nouns
      ce.topics = topics
      ce.highlightTopic = highlightTopic
      ce.site = "local"
      ce.showFirst = showFirst

      PollDAO.save(p)
      
      if (protect) {
        for (a2 <- approval ask ProtectEntry(ce itself)) {
        	ce.protect = true
        	ContentEntryDAO.save(ce)
        }
      } else {
    	  ContentEntryDAO.save(ce)        
      }
      
      ce
    }
  }

  def savePoll(p:Poll, approval:PermTok) = {
    for (a <- approval ask EditPoll(p itself)) yield {
      // TODO: Change polls from HTML to Markdown
      p.question.foreach(str => p.question = Sanitizer.clean(str).toOption)
      p.options = p.options.map ( str =>
        Sanitizer.clean(str) match {
          case RefItself(s) => s
          case RefFailed(exc) => "Failed to clean HTML: " + exc.getMessage
          case _ => ""
        }
      )

      PollDAO.save(p)
      p
    }
  }
  
  def respondToPoll(pollRef:Ref[Poll], tok:PermTok, session:Option[String], answer:Set[Int]) = {
    for (a <- tok ask RespondToPoll(pollRef)) yield {
      val pr = PollResponseDAO.findVote(pollRef, tok.who, session) getOrElse PollResponseDAO.newPollResponse(pollRef, tok.who, session, answer)
      pr.answer = answer
      pr.lastUpdated = new java.util.Date
      PollResponseDAO.save(pr)
      pr
    }
  }

  def newTextPollEntry(book:Ref[Book], approval:Approval[Reader],
    name:Option[String] = None, shortDescription:Option[String] = None,
    adjectives:Set[String] = Set.empty, nouns:Set[String] = Set.empty, topics:Set[String] = Set.empty, highlightTopic:Option[String]= None,
    showFirst:Boolean = false, protect:Boolean = false,
    text:String
  ):Ref[ContentEntry] = {
    for (a <- approval ask AddContent(book)) yield {
      val ce = ContentEntryDAO.newContentEntry(book, approval.who, CEKinds.TextPoll.toString)
      val p = TextPollDAO.newPoll(book, RefItself(ce), approval.who, text)

      ce.refVal = Some(p.id.toString)
      ce.name = name
      ce.shortDescription = shortDescription
      ce.adjectives = adjectives
      ce.nouns = nouns
      ce.topics = topics
      ce.highlightTopic = highlightTopic
      ce.site = "local"
      ce.showFirst = showFirst

      TextPollDAO.save(p)
      
      if (protect) {
        for (a2 <- approval ask ProtectEntry(ce itself)) {
        	ce.protect = true
        	ContentEntryDAO.save(ce)
        }
      } else {
    	  ContentEntryDAO.save(ce)        
      }
      
      ce
    }
  }  
  
  def saveTextPoll(p:TextPoll, approval:PermTok) = {
    for (a <- approval ask EditContent(p.ce)) yield {      
      TextPollDAO.save(p)
      p
    }
  }  

  val wmatch = "[a-zA-Z]+".r
    //val words = for (w <- wmatch.findAllIn(text) if w.length > 1) yield w.toLowerCase
    //

  def respondToTextPoll(pollRef:Ref[TextPoll], tok:PermTok, session:Option[String], answer:String, indexWords:Set[String]) = {
    (for (a <- tok ask RespondToTextPoll(pollRef)) yield {
      TextPollDAO.addAnswer(pollRef, tok.who, answer, indexWords)      
    }).flatten
  }  
  
  
}
