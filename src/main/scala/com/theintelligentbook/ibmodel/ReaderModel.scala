package com.theintelligentbook.ibmodel

import com.wbillingsley.handy._
import mongo._

/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 11/06/12
 * Time: 10:13 PM
 * To change this template use File | Settings | File Templates.
 */

object ReaderModel {

  def findOrCreateByIdent(kind:String, provider:String, value:String) = {
    val refId = IdentificationDAO.find(kind, provider, value) match {
      case RefNone => {
        val i = IdentificationDAO.newIdentification(kind, provider, value)
        IdentificationDAO.save(i)
        RefItself(i)
      }
      case RefItself(i) => RefItself(i)
      case r:RefFailed => r
    }

    val refU = refId.flatMap(_.owner) match {
      case RefNone => {
        val r = ReaderDAO.newReader
        if (ReaderDAO.noReaders) {
          for (role <- SiteRole.values()) {
            r.addSiteRole(role)
          }
        }
        ReaderDAO.save(r)

        refId.foreach { i => i.owner = RefItself(r); IdentificationDAO.save(i) }
        RefItself(r)
      }
      case r => r
    }
    refU
  }

}
