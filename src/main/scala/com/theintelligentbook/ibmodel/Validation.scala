package com.theintelligentbook.ibmodel

import mongo._
import Imports._
import com.wbillingsley.handy._

import java.net.{URISyntaxException, URI}
import RefConversions._

/**
 * Gathers validation failures
 */

case class ValidationFailedException(validation:Validation) extends Exception

object Validation {
  implicit def failureToValidation(f:ValidationFailure):Validation = new Validation & f

  /** Validate a content entry for saving. */
  def getSite(url:String):Ref[String] = {
    try {
      val uri = new URI(url);
      if (uri.isAbsolute) {
        var site = uri.getHost
        if (site == null) {
          RefFailed(ValidationFailedException(ValidationFailure("url","Malformed URL")))
        } else if (site.startsWith("www.")) {
          site = site.substring(4)
          RefItself(site)
        } else {
          RefNone
        }
      } else {
        RefNone
      }
    } catch {
      case ex:URISyntaxException =>
        RefFailed(ex)
    }
  }
  
  
  
  
}

class Validation {
  
  var failures = List.empty[ValidationFailure]

  def passed = failures.isEmpty
  
  def firstFailure = failures.head

  /** Append a failure if there is one */
  def & (opt:Option[ValidationFailure]):Validation = {
    opt.map(f => &(f))
    this
  }

  /** Append a failure */
  def & (f:ValidationFailure):Validation = {
    failures = f :: failures
    this
  }

}

case class ValidationFailure(field:String, msg:String)
