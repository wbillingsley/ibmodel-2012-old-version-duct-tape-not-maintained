package com.theintelligentbook.ibmodel

import mongo._
import com.theintelligentbook.ibmodel.Imports._
import com.wbillingsley.handy._
import SecurityModel._
import Ref._

/**
 * Created with IntelliJ IDEA.
 * User: wbillingsley
 * Date: 20/06/12
 * Time: 9:52 PM
 * To change this template use File | Settings | File Templates.
 */

object PresentationModel {

  def makePresentation(book:Ref[Book], approval:Approval[Reader], includingCE:Ref[ContentEntry]) = {
    for (a <- approval ask AddContent(book)) yield {

      val ce = ContentEntryDAO.newContentEntry(book, approval.who, CEKinds.Presentation.toString)
      val p = PresentationDAO.newPresentation(book, RefItself(ce))
      val inclIds = includingCE.fetch.toSeq.map(_.id)
      p.entries = new RefManyById(classOf[ContentEntry], inclIds)
      PresentationDAO.save(p)

      ce.refVal = Some(p.id.toString)
      for (incl <- includingCE) {
        ce.adjectives = incl.adjectives
        ce.nouns = Set(CEKinds.Presentation.toString)
        ce.topics = incl.topics
        ce.highlightTopic = incl.highlightTopic
      }
      ContentEntryDAO.save(ce)
      p
    }
  }

  /**
   * Creates a new presentation and associated content entry
   * @param book
   * @param tok
   * @param entries
   * @param name
   * @param shortDescription
   * @param adjectives
   * @param nouns
   * @param topics
   * @param highlightTopic
   * @param showFirst
   * @param protect
   * @return
   */
  def newPresentationEntry(book:BookRef, approval:Approval[Reader], entries:RefManyById[ContentEntry, _],
                  name:Option[String], shortDescription:Option[String],
                  adjectives:Set[String], nouns:Set[String], topics:Set[String], highlightTopic:Option[String],
                  showFirst:Boolean, protect:Boolean
  ) = {
    for (a <- approval ask AddContent(book)) yield {
      val ce = ContentEntryDAO.newContentEntry(book, approval.who, CEKinds.Presentation.toString)
      val p = PresentationDAO.newPresentation(book, RefItself(ce))
      p.entries = entries
      PresentationDAO.save(p)

      ce.refVal = Some(p.id.toString)
      ce.name = name
      ce.shortDescription = shortDescription
      ce.adjectives = adjectives
      ce.nouns = nouns
      ce.topics = topics
      ce.highlightTopic = highlightTopic
      ce.site = "local"      
      ce.showFirst = showFirst
      
      if (protect) {
        for (a2 <- approval ask ProtectEntry(ce itself)) { // Note that this might result in a Future
          ce.protect = true 
          ContentEntryDAO.save(ce)       
        }        
      } else {        
        ContentEntryDAO.save(ce)       
      }
      p
    }
  }

  def removeEntriesFromPresentation(pres:Ref[Presentation], indices:Seq[Int], approval:Approval[Reader]):Ref[Presentation] = {
    for (
        a <- approval ask EditPresentation(pres);
        p <- pres
    ) yield {
        val oldIds = p.entries.getIds
        
        val result = for ((id, idx) <- oldIds.zipWithIndex if (!indices.contains(idx))) yield id        
        p.entries = new RefManyById(classOf[ContentEntry], result)
        PresentationDAO.save(p)
        p
    }
  }

  def moveEntriesInPresentation(pres:Ref[Presentation], fromStart:Int, fromEnd:Int, to:Int, approval:Approval[Reader]):Ref[Presentation] = {
    for (
        a <- approval ask EditPresentation(pres);
        p <- pres
    ) yield {
        val ids = p.entries.getIds
        val toMove = ids.slice(fromStart, fromEnd)
        val subtracted = ids.slice(0, fromStart) ++ ids.slice(fromEnd, ids.length)
        val result = subtracted.slice(0, to) ++ toMove ++ subtracted.slice(to, ids.length)
        p.entries = new RefManyById(classOf[ContentEntry], result)
        PresentationDAO.save(p)
        p
    }
  }

  def insertEntriesIntoPresentation(pres:Ref[Presentation], entries:RefManyById[ContentEntry, _], at:Int, approval:Approval[Reader]):Ref[Presentation] = {
    for (
        a <- approval ask EditPresentation(pres);
        p <- pres
    ) yield {
        if (at >= 0) {
          val ids = p.entries.getIds
          val result = ids.slice(0, at) ++ entries.getIds ++ ids.slice(at, ids.length)
          p.entries = new RefManyById(classOf[ContentEntry], result)
        } else {
          p.entries = new RefManyById(classOf[ContentEntry], p.entries.getIds ++ entries.getIds)
        }
        PresentationDAO.save(p)
        p
    }
  }


}