package com.theintelligentbook.ibmodel

import mongo._
import Imports._
import com.wbillingsley.handy._

import _root_.name.fraser.neil.plaintext.diff_match_patch
import _root_.java.util.Date

import _root_.org.owasp.validator.html.AntiSamy;
import _root_.org.owasp.validator.html.Policy;
import SecurityModel._
import Ref._

object ChatModel extends Loggable {

  def addChatComment(
    book:Ref[Book], approval:Approval[Reader], anonymous:Boolean,
    session:Option[String], content:Option[String],
    topics:Seq[String]
  ) = {
    for (a <- approval ask Chat(book)) yield {
      val c = ChatCommentDAO.newChatComment(anonymous, book, approval.who, session, content, topics)
      ChatCommentDAO.save(c)
      c      
    }
  }

  def getRecentChatComments(book:Ref[Book], after:Ref[ChatComment], before:Ref[ChatComment], maxElements:Option[Int], approval:Approval[Reader]) = {
    (approval ask Read(book)) flatMap { a =>
      ChatCommentDAO.findByBook(book, after, before, maxElements) toRefMany
    }
  }

  
  /**
   * Runs a map-reduce on the database to aggregate hourly summaries of chat
   * comments. 
   * 
   * This is temporary -- when we've worked out what formats we need, we'll 
   * update the stats dynamically as chats arrive.
   */
  def updateAggregatedChats(b:Ref[Book], approval:Approval[Reader]) {
    for (a <- approval ask ViewStats(b)) {
      ChatCommentDAO.updateStatsByDay(b)
    }
  }
  
  /**
   * Fetches aggregates chat data to send to the client
   */
  def aggregatedChats(b:Ref[Book], approval:Approval[Reader]) = {
    (approval ask ViewStats(b)) flatMap { a =>
      AggChatDAO.aggEventsJson(b:Ref[Book]) toRefMany
    } 
  }

}
