package com.theintelligentbook.ibmodel

import mongo._
import Imports._
import com.wbillingsley.handy._
import java.net.{URISyntaxException, URI}
import SecurityModel._
import Ref._

object ExternalsModel {

  /**
   * Adds a new ContentEntry for a web page
   */
  def newWebsiteEntry(bookRef:BookRef, tok:PermTok, url:String,
                  name:Option[String] = None, shortDescription:Option[String] = None,
                  adjectives:Set[String] = Set.empty, nouns:Set[String] = Set.empty, topics:Set[String] = Set.empty, highlightTopic:Option[String] = None,
                  showFirst:Boolean = false, protect:Boolean = false
                   ) = {
    for (a <- tok ask AddContent(bookRef)) yield {
      val ce = ContentEntryDAO.newContentEntry(bookRef, tok.who, CEKinds.URL.toString)
      val w = WebsiteDAO.newWebsite(bookRef, RefItself(ce), Some(url))

      ce.name = name
      ce.shortDescription = shortDescription
      ce.adjectives = adjectives
      ce.nouns = nouns
      ce.topics = topics
      ce.highlightTopic = highlightTopic
      ce.refVal = Some(w.id.toString)
      try {
        val uri = new URI(url);
        if (uri.isAbsolute) {
          ce.site = uri.getHost
          if (ce.site == null) {
            ce.site = "(malformed url)"
          } else if (ce.site.startsWith("www.")) {
            ce.site = ce.site.substring(4)
          }
        } else {
          ce.site = "local"
        }
      } catch {
        case ex:URISyntaxException =>
          ce.site = "(malformed url)"
      }
      ce.showFirst = showFirst

      WebsiteDAO.save(w)
      if (protect) { 
        for (a2 <- tok ask ProtectEntry(ce.itself)) {
        	ce.protect = true
        	ContentEntryDAO.save(ce)
        }
      } else {
        ContentEntryDAO.save(ce)
      }      
      ce
    }
  }

  /**
   * Adds a new ContentEntry for a web page
   */
  def newYouTubeEntry(bookRef:BookRef, tok:PermTok, youtubeId:String,
                      name:Option[String], shortDescription:Option[String],
                      adjectives:Set[String], nouns:Set[String], topics:Set[String], highlightTopic:Option[String],
                      showFirst:Boolean, protect:Boolean
                       ) = {
    for (a <- tok ask AddContent(bookRef)) yield {
      val ce = ContentEntryDAO.newContentEntry(bookRef, tok.who, CEKinds.YouTube.toString)
      val w = YouTubeDAO.newYouTube(bookRef, RefItself(ce), Some(youtubeId))

      ce.name = name
      ce.shortDescription = shortDescription
      ce.adjectives = adjectives
      ce.nouns = nouns
      ce.topics = topics
      ce.highlightTopic = highlightTopic
      ce.refVal = Some(w.id.toString)
      ce.site = "youtube.com"
      ce.showFirst = showFirst

      YouTubeDAO.save(w)
      if (protect) { 
        for (a2 <- tok ask ProtectEntry(ce.itself)) {
        	ce.protect = true
        	ContentEntryDAO.save(ce)
        }
      } else {
        ContentEntryDAO.save(ce)
      }      
      ce
    }
  }
  
  /**
   * Adds a new ContentEntry for a GooglePresentation
   */
  def newGoogleSlidesEntry(bookRef:BookRef, tok:PermTok, embedCode:Option[String], presId:Option[String],
                      name:Option[String] = None, shortDescription:Option[String] = None,
                      adjectives:Set[String] = Set.empty, nouns:Set[String] = Set.empty, topics:Set[String] = Set.empty,
                       highlightTopic:Option[String] = None,
                      showFirst:Boolean, protect:Boolean
                       ) = {
    for (a <- tok ask AddContent(bookRef)) yield {
      val ce = ContentEntryDAO.newContentEntry(bookRef, tok.who, CEKinds.GoogleSlides.toString)
      val w = GoogleSlidesDAO.newGooglePres(bookRef, RefItself(ce), embedCode, presId)

      ce.name = name
      ce.shortDescription = shortDescription
      ce.adjectives = adjectives
      ce.nouns = nouns
      ce.topics = topics
      ce.highlightTopic = highlightTopic
      ce.refVal = Some(w.id.toString)
      ce.site = "drive.google.com"
      ce.showFirst = showFirst

      GoogleSlidesDAO.save(w)
      if (protect) { 
        for (a2 <- tok ask ProtectEntry(ce.itself)) {
        	ce.protect = true
        	ContentEntryDAO.save(ce)
        }
      } else {
        ContentEntryDAO.save(ce)
      }      
      ce
    }
  }  
  

  def saveWebsite(w:Website, tok:PermTok) = {
    for (a <- tok ask EditContent(w.ce)) yield {
      WebsiteDAO.save(w)
      w
    }
  }

  def saveYouTube(w:YouTube, tok:PermTok) = {
    for (a <- tok ask EditContent(w.ce)) yield {
      YouTubeDAO.save(w)
      w
    }
  }

  def saveGooglePres(w:GoogleSlides, tok:PermTok) = {
    for (a <- tok ask EditContent(w.ce)) yield {
      GoogleSlidesDAO.save(w)
      w
    }
  }
}
