/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.theintelligentbook.ibmodel

import mongo._
import Imports._
import com.wbillingsley.handy._
import Ref._

import _root_.name.fraser.neil.plaintext.diff_match_patch
import _root_.java.util.Date

import _root_.org.owasp.validator.html.AntiSamy;
import _root_.org.owasp.validator.html.Policy;
import SecurityModel._

object WikiModel extends Loggable {
  
  case class PatchException(msg:String) extends Exception(msg)
  case class VersionException(msg:String) extends Exception(msg)
  
  private val diff_match_patch = new diff_match_patch
  
  /**
   * Calculates a patch string for transforming v1 to v2
   */
  private def calcPatch(v1:String, v2:String) = {
    val diffs = diff_match_patch.diff_main(v1, v2, true);
    diff_match_patch.diff_cleanupSemantic(diffs)
    val patchList = diff_match_patch.patch_make(v1, diffs)
    diff_match_patch.patch_toText(patchList)
  }
  
  /**
   * Applies a patch to v1, and returns a box containing v2 or a reason for failure
   */
  private def applyPatch(v1:String, patch:String) = {
    val patches = diff_match_patch.patch_fromText(patch)
    val results = diff_match_patch.patch_apply(patches, v1)
    
    val v2 = results(0).toString
    
    results(1) match {
      case bArr:Array[Boolean] => {
        val success = bArr.reduceLeft(_ && _)

        if (success) {
          RefItself(v2)
        } else {
          RefFailed(PatchException("Some patches could not be applied"))
        }          
      }
      case _ => RefFailed(PatchException("A type error occured when trying to apply patches"))
    }    
  }

  def read(pageRef: Ref[WikiPage], tok:PermTok) = {
    for (
        a <- tok ask Read(pageRef.flatMap(_.book));
        p <- pageRef
    ) yield p
  }


  private def int_setPageContent(pageRef: Ref[WikiPage], readerRef: Ref[Reader], content: String, comment: Option[String]) = {
    logger.debug("Dirty content is " + content)
    Sanitizer.clean(content).flatMap { cleanContent =>
      pageRef.map { wikiPage =>

        val forwardDiff = calcPatch(wikiPage.content, cleanContent)
        val reverseDiff = calcPatch(cleanContent, wikiPage.content)
        val version = wikiPage.version + 1
        val date = new Date()

        val h = WikiPageHistoryDAO.newWikiPageHistory(RefItself(wikiPage), readerRef, Some(forwardDiff), Some(reverseDiff), version, comment)
        WikiPageHistoryDAO.save(h)

        wikiPage.version = version
        wikiPage.lastUpdated = date
        wikiPage.content = cleanContent
        WikiPageDAO.save(wikiPage)

        h
      }
    }
  }

  /**
   * Applies a patch to a wiki page
   * @return Box containing the new version number of the page, or a reason for failure
   */
  def patchPage(pageRef: Ref[WikiPage], tok:PermTok, patch:String, lastVersion: Long, comment: Option[String]) = {
    (tok ask EditWikiPage(pageRef)) flatMap { a =>
      pageRef.flatMap { wikiPage =>
        if (wikiPage.version != lastVersion) {
          RefFailed(VersionException("Latest version did not match the version provided"))
        } else {
          applyPatch(wikiPage.content, patch) match {
            case rn:RefNothing => RefFailed(PatchException("Patching failed"))
            case RefItself(newContent) => {
              int_setPageContent(RefItself(wikiPage), tok.who, newContent, comment)
            }
          }
        }
      }
    }
  }

  /**
   * Sets the content of a page
   * @return Box containing the page's new version number, or a reason for failure
   */
  def setPageContent(pageRef: Ref[WikiPage], tok: PermTok, content:String, lastVersion: Long = -1, comment: Option[String] = None) = {
    (tok ask EditWikiPage(pageRef)) flatMap { a =>
      pageRef.flatMap { page =>
        if (lastVersion >= 0 && page.version != lastVersion) {
          RefFailed(VersionException("Latest version did not match the version provided"))
        } else {
          int_setPageContent(RefItself(page), tok.who, content, comment)
        }
      }
    }
  }

  /**
   * Adds a new ContentEntry for a wiki page
   * @param bookRef
   * @param tok
   * @param content
   * @param name
   * @param shortDescription
   * @param adjectives
   * @param nouns
   * @param topics
   * @param showFirst
   * @param protect
   * @return
   */
  def newPageEntry(bookRef:BookRef, tok:PermTok, content:String,
                  name:Option[String], shortDescription:Option[String],
                  adjectives:Set[String], nouns:Set[String], topics:Set[String], highlightTopic:Option[String],
                  showFirst:Boolean, protect:Boolean
  ):Ref[ContentEntry] = {
    for (
        a <- tok ask AddContent(bookRef);
        cleanContent <- Sanitizer.clean(content)
    ) yield {
        val ce = ContentEntryDAO.newContentEntry(bookRef, tok.who, CEKinds.WikiPage.toString)
        val w = WikiPageDAO.newWikiPage(bookRef, RefItself(ce), tok.who, RefNone)
        w.content = cleanContent

        ce.refVal = Some(w.id.toString)
        ce.name = name
        ce.shortDescription = shortDescription
        ce.adjectives = adjectives
        ce.nouns = nouns
        ce.topics = topics
        ce.highlightTopic = highlightTopic
        ce.site = "local"
         ce.showFirst = showFirst

        WikiPageDAO.save(w)
  
        if (protect) {
          for (a2 <- tok ask ProtectContent(bookRef)) {
            ce.protect = true
            ContentEntryDAO.save(ce)
          }          
        } else {
          ContentEntryDAO.save(ce)
        }
         
        ce
    }
  }
  
  
  /**
   * Adds a new ContentEntry for a wiki page
   * @param bookRef
   * @param tok
   * @param content
   * @param name
   * @param shortDescription
   * @param adjectives
   * @param nouns
   * @param topics
   * @param showFirst
   * @param protect
   * @return
   */
  def newMarkupPageEntry(book:Ref[Book], tok:Approval[Reader], content:String,
                  name:Option[String], shortDescription:Option[String],
                  adjectives:Set[String], nouns:Set[String], topics:Set[String], highlightTopic:Option[String],
                  showFirst:Boolean, protect:Boolean
  ):Ref[ContentEntry] = {
    for (
        a <- tok ask AddContent(book)
        
    ) yield {
        val ce = ContentEntryDAO.newContentEntry(book, tok.who, CEKinds.MarkupWikiPage.toString)
        val w = MarkupWikiPageDAO.newWikiPage(book, RefItself(ce), tok.who, content)        

        println(w.content)
        
        ce.refVal = Some(w.id.toString)
        ce.name = name
        ce.shortDescription = shortDescription
        ce.adjectives = adjectives
        ce.nouns = nouns
        ce.topics = topics
        ce.highlightTopic = highlightTopic
        ce.site = "local"
        ce.showFirst = showFirst

        MarkupWikiPageDAO.save(w)
  
        if (protect) {
          for (a2 <- tok ask ProtectContent(book)) {
            ce.protect = true
            ContentEntryDAO.save(ce)
          }          
        } else {
          ContentEntryDAO.save(ce)
        }
         
        ce
    }
  }
  
  private def int_setMarkupPageContent(pageRef: Ref[MarkupWikiPage], readerRef: Ref[Reader], content: String, comment: Option[String]) = {
      pageRef.map { wikiPage =>

        val forwardDiff = calcPatch(wikiPage.content, content)
        val reverseDiff = calcPatch(content, wikiPage.content)
        val version = wikiPage.version + 1
        val date = new Date()

        //val h = WikiPageHistoryDAO.newWikiPageHistory(RefItself(wikiPage), readerRef, Some(forwardDiff), Some(reverseDiff), version, comment)
        //WikiPageHistoryDAO.save(h)

        wikiPage.version = version
        wikiPage.lastUpdated = date
        wikiPage.content = content
        MarkupWikiPageDAO.save(wikiPage)

        wikiPage
      }
  }  
  
  /**
   * Sets the content of a page
   * @return Box containing the page's new version number, or a reason for failure
   */
  def setMarkupPageContent(page: Ref[MarkupWikiPage], tok: Approval[Reader], content:String, lastVersion: Long = -1, comment: Option[String] = None) = {
    (for (p <- page; approval <- tok ask EditContent(p.ce)) yield {
	    if (lastVersion >= 0 && p.version != lastVersion) {
	      RefFailed(VersionException("Latest version did not match the version provided"))
	    } else {
	      int_setMarkupPageContent(p.itself, tok.who, content, comment)
	    }      
    }).flatten    
  }  
  
}

/**
 * Sanitizes user-submitted HTML
 */
object Sanitizer {
  
  case class SanitizerException(msg:String) extends Exception(msg)
  
  var policy:Option[Policy] = None
  
  def setPolicyFromResource(path: String) {
    val p = Policy.getInstance(getClass.getResource(path))
    policy = Some(p)
  }
  
  def clean(dirty:String):ResolvedRef[String] = {
    policy match {
      case Some(p) => {
        val antiSamy = new AntiSamy
        try {
          val cr = antiSamy.scan(dirty, p, AntiSamy.SAX)
          RefItself(cr.getCleanHTML)
        } catch {
          case ex:Exception => RefFailed(ex)
        }
      }
      case None => {
          RefFailed(SanitizerException("No HTML sanitising policy found"))
      }
    }
  }

}
