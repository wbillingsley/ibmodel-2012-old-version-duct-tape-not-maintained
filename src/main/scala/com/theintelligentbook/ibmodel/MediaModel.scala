package com.theintelligentbook.ibmodel

import mongo._
import Imports._
import com.wbillingsley.handy._

import java.io.InputStream

import RefConversions._
import java.util.zip.{ZipEntry, ZipInputStream}
import collection.mutable.{Buffer, ArrayBuffer}
import scala.util.matching.Regex
import SecurityModel._
import Ref._

object MediaModel extends Loggable {

  /**
   * Detects if a sequence of (id, name) fits a pattern of {name}{number} or {name}{number}.{extension} and if so,
   * sorts them by number.  So, for instance img10.png would come after img2.png, not before.
   */
  def sortByNumberedNames(files:Seq[MediaFile]) = {
    val pattern = new Regex("(.*[^0-9])?([0-9]+)(\\..+)?", "stem", "number", "suffix").pattern
    if (files.forall { mf => pattern.matcher(mf.name.getOrElse("")).matches}) {
      files.sortBy { mf =>
        val matcher = pattern.matcher(mf.name.getOrElse(""))
        matcher.find
        matcher.group(2).toInt
      }
    } else {
      files
    }
  }

  def splitName(nameOpt:Option[String]) = {
    nameOpt match {
      case Some(name) => {
        val lastDot = name.lastIndexOf('.');
        val suffix = if (lastDot >= 0) {
          Some(name.substring(lastDot + 1).toLowerCase)
        } else {
          None
        }
        val prefix = if (lastDot >=0) {
          Some(name.substring(0, lastDot))
        } else {
          Some(name)
        }
        (prefix, suffix)
      }
      case None => (None, None)
    }
  }



  def storeImage(
    book:Ref[Book],
    permTok:PermTok,
    mimeType:Option[String] = None,
    name:Option[String] = None,
    shortDescription:Option[String] = None,
    public:Boolean = false,
    inputStream:InputStream
  ) = {
    for (a <- permTok ask UploadMedia(book)) yield {
      val (prefix, suffix) = splitName(name)
      val mf = MediaFileDAO.newMediaFile(
        book=book, addedBy=permTok.who,
        mimeType=mimeType, extension=suffix,
        name=prefix, shortDescription=shortDescription
      )
      mf.public = public;
      MediaFileDAO.save(mf)
      MediaFileDAO.saveStream(mf, inputStream)
      mf
    }
  }

  /**
   * Stores a zip of images.
   */
  def storeZip(
    book:Ref[Book],
    permTok:Approval[Reader],
    zipInputStream:ZipInputStream
  ):RefMany[MediaFile] = {
    (permTok ask UploadMedia(book)) flatMap { a =>

      // Keep a list of the images we have stored
      val storedImages = Buffer.empty[MediaFile]

      var zipEntry:ZipEntry = null;
      while ({zipEntry = zipInputStream.getNextEntry; zipEntry != null}) {

        var name = zipEntry.getName;
        var directory = ""
        val lastSlash = name.lastIndexOf('/');
        if (lastSlash >= 0) {
          directory = name.substring(0, lastSlash);
          name = name.substring(lastSlash + 1);
        }

        if (name.startsWith(".")) {
          logger.debug(name + " is a hidden file")
        } else {
          val (prefix, suffix) = splitName(Some(name))

          val storeMime = suffix match {
            case Some("png") => Some("image/png")
            case Some("jpg") => Some("image/jpeg")
            case Some("jpeg") => Some("image/jpeg")
            case Some("gif") => Some("image/gif")
            case _ => None
          };

          for (mime <- storeMime) {
            val mf = MediaFileDAO.newMediaFile(
              book, permTok.who,
              Some(mime), suffix,
              prefix, shortDescription=None
            )
            MediaFileDAO.save(mf)
            MediaFileDAO.saveStream(mf, zipInputStream)

            storedImages.append(mf)            
          }
        }
      }
      RefTraversableOnce(storedImages)
    }
  }

  def getStream(mediaFile:MediaFile) = MediaFileDAO.getStream(mediaFile)

  def getImage(mediaFile:Ref[MediaFile], tok:PermTok) = {
    (tok ask ViewMediaFile(mediaFile)) flatMap { a => mediaFile }
  }

  def newContentEntriesFromZip(
    book:Ref[Book],
    tok:PermTok,
    zipInputStream:ZipInputStream,
    adjectives:Set[String], nouns:Set[String], topics:Set[String], highlightTopic:Option[String],
    showFirst:Boolean, protect:Boolean
  ) = {    
    val approved = if (protect) tok ask (AddContent(book), ProtectContent(book)) else tok ask AddContent(book)
    
    approved flatMap { a => 
      val storedImagesRef = storeZip(book, tok, zipInputStream)
      val refSeqCe = storedImagesRef.map { image =>
          val ce = ContentEntryDAO.newContentEntry(book, tok.who, CEKinds.MediaFile.toString)
          ce.refVal = Some(image.id.toString)
          ce.name = image.name
          ce.adjectives = adjectives
          ce.nouns = nouns
          ce.topics = topics
          ce.highlightTopic = highlightTopic
          ce.site = "local"
          if (protect) {
            ce.protect = true
          }
          ce.showFirst = showFirst
          ContentEntryDAO.save(ce)
          ce
      }
      refSeqCe
    } 
  }

}
