package com.theintelligentbook.ibmodel

import mongo._
import Imports._
import com.wbillingsley.handy._

/**
 * Converters between particular kinds of reference
 */
object RefConversions {

  implicit def readerRefToApproval(rr:Ref[Reader]) = new Approval(rr)

  implicit class EntryRefOps(val er:Ref[ContentEntry]) extends AnyVal {
	  
	def book: Ref[Book] = er.flatMap(_.book)
	
	def refVal:String = er.toOption.flatMap(_.refVal).getOrElse("(None)")
	
	def site:String = er.toOption.map(_.site).getOrElse("(None)")
	  
	def name:String = er.toOption.flatMap(_.name).getOrElse("(Unnamed)")
	  
	def shortDesc:String = er.toOption.flatMap(_.shortDescription).getOrElse("(None)")
	
  }

}

