package com.theintelligentbook.ibmodel

/**
 * Created by IntelliJ IDEA.
 * User: wbillingsley
 * Date: 28/04/11
 * Time: 5:37 PM
 */

import mongo._
import Imports._
import com.theintelligentbook.ibmodel.mongo.Imports._
import com.wbillingsley.handy._
import Ref._

import _root_.scala.collection.mutable

import RefConversions._
import java.net.{URISyntaxException, URI}
import org.bson.types.ObjectId
import SecurityModel._

/**
 * Point of entry for making changes to ContentEntries.
 */
object ContentModel {
  
  val defaultTopic = "page one"

  /**
   * Fetches all the alternative entries to a content entry
   */
  def getAlternatives(entryRef: EntryRef, req: PermTok):RefMany[ContentEntry] = {
    getAlternatives(entryRef.book, entryRef, req)
  }

  /**
   * Fetches all the alternative entries to a content entry
   */
  def getAlternatives(book:Ref[Book], entry: Ref[ContentEntry], approval:Approval[Reader]):RefMany[ContentEntry] = {
    for (
        a <- approval ask Read(book);
        e <- entry;
        topic <- Ref.fromOptionItem(e.highlightTopic);
        t <- ContentEntryDAO.entriesByTopic(book, topic).toRefMany
    ) yield t
  }
  
  /**
   * Creates a new content entry if allowed.
   */
  def newContentEntry(book:Ref[Book], approval:Approval[Reader], kind:String) = {
    for (a <- approval ask AddContent(book)) yield {
      ContentEntryDAO.newContentEntry(book, approval.who, kind)
    }
  }

  /**
   * Validates and saves a new or created content entry if allowed.
   **/
  def saveContentEntry(entry:ContentEntry, tok:PermTok):Ref[ContentEntry] = {

    val approved = if (entry.isNew) {      
      if (entry.protect) {
        tok ask (AddContent(entry.book), ProtectContent(entry.book))
      } else {
        tok ask AddContent(entry.book)
      }         
    } else {      
      if (entry.protect) {
        tok ask (EditContent(RefById(classOf[ContentEntry], entry.id)), ProtectContent(entry.book))
      } else {
        tok ask EditContent(RefById(classOf[ContentEntry], entry.id))
      }       
    }
    
    for (a <- approved) yield {
      ContentEntryDAO.save(entry)
      entry        
    }
  }


  /**
   * Return true if the ContentEntry matches all filters
   * @param p
   * @param filters
   * @return
   */
  private def applyFilters(p:ContentEntry, filters:Map[String,String]):Boolean = {
    var v = true
    for ((key, value) <- filters) {
      key match {
        case "noun" => v &&= p.nouns.contains(value)
        case "adjective" => v &&= p.adjectives.contains(value)
        case "site" => v &&= (value == p.site)
        case _ => true
      }
    }
    v
  }

  /**
   * Chooses one of a Seq of ContentEntries to prefer.
   * @param list
   * @return
   */
  private def pick(list:Seq[ContentEntry]) = {
    require(list.length > 0, "Picking from list of 0 entries")

    var fav:ContentEntry = null
    for (ce <- list) {
      if (
        (fav == null) ||
          (ce.showFirst && !fav.showFirst) ||
          (!(fav.showFirst && !ce.showFirst) && (
            (CEKinds.valueOf(ce.kind).compareTo(CEKinds.valueOf(fav.kind)) > 0)
            ))
      ) {
        fav = ce
      }
    }
    fav
  }

  /**
   * Retrieves a Seq of ContentEntries for a given topic matching a set of filters.
   * As this method is normally not the first entry point of a client (usually the client
   * will have looked up a ContentEntry first), security failures merely return an empty
   * Seq rather than an error.
   * @param bookRef
   * @param tok
   * @param topic
   * @param filters
   * @return
   */
  def filteredEntriesByTopic(bookRef:BookRef, tok:PermTok, topic:Option[String], filters:Map[String,String]) = {
    for (
        approved <- tok ask Read(bookRef);
        e <- (ContentEntryDAO.entriesByTopic(bookRef, topic.getOrElse(defaultTopic)).toRefMany) if applyFilters(e, filters)
    ) yield e
  }

  /**
   * New style lookup now that we have Presentations.
   * @param bookRef
   * @param tok
   * @param topic
   * @param filters
   * @return
   */
  def recommendCE(bookRef:BookRef, tok:PermTok, topic:Option[String], filters:Map[String,String]):Ref[ContentEntry] = {
    for (approved <- tok ask Read(bookRef)) yield {
      val all = ContentEntryDAO.entriesByTopic(bookRef, topic.getOrElse(defaultTopic))
      val filtered = all.filter(applyFilters(_, filters))
      pick(filtered)
    } 
  }

  /**
   * Given a ContentEntry, see if there's a Presentation context in which to show it.
   * If the ContentEntry is a presentation, this means opening it to the first slide.
   * If the ContentEntry is not a presentation, this means looking for presentations
   * it is in.
   * @param ceRef
   * @return
   */  
  def presAndCE(entry:Ref[ContentEntry], pres:Ref[Presentation] = RefNone):(Ref[ContentEntry], Ref[Presentation]) = {
    
    val rtuple = for (
        pOpt <- optionally(pres);
        ce <- entry orIfNone Ref(pOpt).flatMap(_.start)  // If no entry, default to first entry in sequence
    ) yield {      
        if (CEKinds.Presentation.toString == ce.kind) {
          // The CE is the presentation.  Try to open it to the first slide.
          val pr = ce.referenceAs(classOf[Presentation])
          (pr.flatMap(_.start), pr)
        } else {           
          // If a presentation context was given, check that the content entry is an entry in it
          val pOpt2 = pOpt.filter(_.entries.getIds contains ce.itself.getId)
          pOpt2 match {
            case Some(p) => (ce.itself, p.itself)
            case None => (ce.itself, Ref(ce.containedIn.headOption))
          }
        }
    }
    
    (rtuple.flatMap(_._1), rtuple.flatMap(_._2)) 
  }
  

  def upVote(ceRef:Ref[ContentEntry], tok:PermTok) = {
    for (
        ce <- ceRef;
        approved <- tok ask VoteOnEntry(ce.itself)
    ) yield {
      ContentEntryDAO.upVote(ceRef, tok.who)
      ce.score + 1
    }    
  }

  def downVote(ceRef:Ref[ContentEntry], tok:PermTok) = {
    for (
        ce <- ceRef;
        approved <- tok ask VoteOnEntry(ce.itself)
    ) yield {
      ContentEntryDAO.downVote(ceRef, tok.who)
      ce.score - 1
    } 
  }  
  
  /**
   * Adds a comment about this content entry
   */
  def comment(ceRef:Ref[ContentEntry], tok:PermTok, session:Option[String], comment:String) {
    for (approved <- tok ask Comment(ceRef)) {
      ContentEntryDAO.addComment(ceRef, tok.who, session, comment)
    }
  }
  
  /**
   * Gets comments about a content entry from the DB, ignoring the cache
   */
  def getComments(ce:Ref[ContentEntry], tok:PermTok, skip:Int=0, limit:Int = 20):RefMany[EntryComment] = {
    val a = (for (        
        appr <- tok ask ReadEntry(ce)
    ) yield {
      EntryCommentDAO.findByCE(ce, skip, limit).toRefMany
    })
    a.flatten
  }
  
  
  def getAllEntriesForTopic(bookRef: BookRef, tok:PermTok, topic:String) = {
    for (
        approved <- tok ask Read(bookRef);
        e <- (ContentEntryDAO.entriesByTopic(bookRef, topic).toRefMany) 
    ) yield e
  }
  

  /*----
   * Private methods that will go into a DB access layer
   *----*/

  /**
   * Checks that all the entries in the sequence are in the same book as each other.
   */
  private def inSameBook(entries:EntryRef*) = {
    if (entries.isEmpty) {
      true
    } else {
      val bookId = entries(0).book.getId
      entries.forall(e => e.book.getId == bookId)
    }
  }
  
  /*
   *  Methods awaiting refactoring...
   */

  /**
   * Takes a list of content entries, and returns a map collecting them together by topic and by the initial letter of the topic.
   */
  def alphabeticIndexByMap(entries: Seq[ContentEntry]) = {

    val alphaMap = mutable.Map.empty[String, mutable.Map[String, Seq[ContentEntry]]]

    for (entry <- entries; topic <- entry.topics) {
      val initial = topic.trim.toUpperCase.substring(0,1)
      if (!alphaMap.contains(initial)) {
        val topicMap = mutable.Map(topic -> Seq(entry))
        alphaMap(initial) = topicMap
      } else {
        alphaMap(initial)(topic) = alphaMap(initial).getOrElse(topic, Seq.empty[ContentEntry]) :+ entry
      }
    }
    alphaMap
  }

  /**
   * Takes a list of content entries, and returns a map collecting them together by topic and by the initial letter of the topic.
   */
  def alphabeticIndexByCE(entries: Seq[ContentEntry]) = {
    val alphaList = _root_.scala.collection.mutable.Map.empty[String, _root_.scala.collection.mutable.Map[String, List[ContentEntry]]]
    for (ce <- entries; topic <- ce.topics) {
      val initial = if (topic == null || topic.isEmpty) { "(empty)" } else { topic.trim.toUpperCase.substring(0,1) }
      if (!alphaList.contains(initial)) {
        val topicMap = _root_.scala.collection.mutable.Map(topic -> List(ce))
        alphaList(initial) = topicMap
      } else {
        alphaList(initial)(topic) = ce :: alphaList(initial).getOrElse(topic, Nil)
      }
    }
    alphaList
  }
  
  def allEntriesInBook(bookRef:BookRef, tok:PermTok) = {
    (tok ask Read(bookRef)) flatMap { approved => 
      ContentEntryDAO.entriesByBook(bookRef).toRefMany
    }
  }

  /**
   * Constructs and runs a query to select chosen properties of ContentEntries that match specified filters.
   * @return List of Maps
   */
  def queryContentEntries(bookRef:BookRef, tok:PermTok, filters: Map[String, Any]):RefMany[ContentEntry] = {
    (tok ask Read(bookRef)) flatMap { approved => 
      ContentEntryDAO.queryContentEntries(bookRef, filters).toRefMany
    } 
  }
  
  /**
   * Returns a seq of the most recently updated content entries
   */
  def recentlyUpdated(bookRef:BookRef, tok:PermTok, max:Option[Int] = Some(30)):RefMany[ContentEntry] = {
    (tok ask Read(bookRef)) flatMap { approved => 
      ContentEntryDAO.recentlyUpdated(bookRef, true, max).toRefMany
    }    
  }

  def allTrashedEntriesInBook(bookRef:BookRef, tok:PermTok) = {
    (tok ask Read(bookRef)) flatMap { approved => 
      ContentEntryDAO.trashedEntriesByBook(bookRef).toRefMany 
    }
  }
  
  def parseTopicString(topic:String) = topic

  def parseTypeString(typ:String) = typ



  
}
