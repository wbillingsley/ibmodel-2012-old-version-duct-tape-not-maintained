package com.theintelligentbook.ibmodel

import mongo._
import Imports._
import com.wbillingsley.handy._

import com.theintelligentbook.liftmongo.comet.{ChatMessage, ChatServer}
import RefConversions._
import net.liftweb.common.{Empty, Failure, Full}


/**
 * Collects the methods relating to chat methods in one place.  (Moved here from other files)
 */
object ChatModel {

  /**
   * Records a chat comment
   */
  def addChatComment(permTok:PermTok, bookRef:BookRef,  topics:Seq[String], comment:String, anonymous:Boolean) = {
    val resolved = SecurityModel.mayChat(bookRef, permTok);
    if (resolved.may) {
      val chatComment = new ChatComment();
      chatComment.comment = comment
      chatComment.anonymous = anonymous   
      chatComment.createdBy = permTok.who.getOrElse(null)
      chatComment.intellibook = bookRef.getOrElse(null)
      val persisted = Model.persist(chatComment)

      for (topic <- topics) {
        val chatCommentTopic = new ChatCommentTopic()
        chatCommentTopic.intellibook = bookRef.getOrElse(null)
        chatCommentTopic.comment = chatComment
        chatCommentTopic.topic = topic
        Model.persist(chatCommentTopic)
      }
      Model.flush

      ChatServer ! new ChatMessage(true, bookRef.toOption.map(_.id), resolved.who.toOption.map(_.id), comment, topics)
      Full(persisted)
    } else {
      new Failure(msg=resolved.reason, exception=Empty, chain=Empty)
    }
  }
  
}
