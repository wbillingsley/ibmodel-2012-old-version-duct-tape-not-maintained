package com.theintelligentbook.ibmodel

/**
 * Created by IntelliJ IDEA.
 * User: wbillingsley
 * Date: 22/02/12
 * Time: 5:27 PM
 * To change this template use File | Settings | File Templates.
 */


import mongo._
import Imports._
import com.wbillingsley.handy._

import RefConversions._


object MinutePaperModel {

  def newMinutePaper(bookRef:BookRef, tok:PermTok):ResolvedRef[OneMinutePaper] = {
    SecurityModel.mayAddContent(bookRef, tok).perform {
      OneMinutePaperDAO.newMinutePaper(bookRef)
    }
  }

  def newMinutePaperResponse(omp:Ref[OneMinutePaper], tok:PermTok, session:Option[String]) = {
    SecurityModel.mayRead(omp.map(_.intellibook), tok).perform {
      OneMinutePaperDAO.newMinutePaperResponse(omp, tok.who, session)
    }
  }
  
  def getResponse(ompRef:Ref[OneMinutePaper], readerRef: ReaderRef, session: Option[String]) = {
    OneMinutePaperDAO.getResponse(ompRef, readerRef, session)
  }
  
  def getResponses(ompRef: Ref[OneMinutePaper], tok: PermTok) = {
    SecurityModel.mayViewStats(ompRef.map(_.intellibook), tok).perform {
      OneMinutePaperDAO.getResponses(ompRef)
    }
  }
  
  def storeResponse(
    bookRef: BookRef,
    ompRef:Ref[OneMinutePaper],
    tok: PermTok,
    session: Option[String],
    rating: Option[Int],
    responseA: Option[String] = None,
    responseB: Option[String] = None,
    responseC: Option[String] = None,
    responseD: Option[String] = None    
  ) = {
    
    val ompr = getResponse(ompRef, tok.who, session) orIfNone newMinutePaperResponse(ompRef, tok, session)
    for (resp <- ompr) {
      resp.rating = rating.getOrElse(-1)
      resp.responseA = responseA.getOrElse("")
      resp.responseB = responseB.getOrElse("")
      resp.responseC = responseC.getOrElse("")
      resp.responseD = responseD.getOrElse("")
      tok.who.toOption.foreach(resp.reader = _)
    }
    ompr
  }

}
