name := "ibmodel"

organization := "com.theintelligentbook"

version  := "0.2-SNAPSHOT"

scalaVersion := "2.10.0"

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")

resolvers += DefaultMavenRepository

resolvers += JavaNet1Repository

resolvers += "repo.novus rels" at "http://repo.novus.com/releases/"

resolvers += "repo.novus snaps" at "http://repo.novus.com/snapshots/"

resolvers += "typesafe snaps" at "http://repo.typesafe.com/typesafe/snapshots/"

resolvers += "sonatype snaps" at "https://oss.sonatype.org/content/repositories/snapshots/"

libraryDependencies += "com.theintelligentbook" %% "ibmongo" % "0.2-SNAPSHOT"

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.6.4"

libraryDependencies += "org.owasp.antisamy" % "antisamy" % "1.4.4"

libraryDependencies += "com.novocode" % "junit-interface" % "0.7" % "test"

crossScalaVersions := Seq("2.10.0")
